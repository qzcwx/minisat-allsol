# nicknaming instance, and generate command.lst

import os
import re



# s = set()

nicknameList = ['rsdecoder-debug',  'sudoku-debug',  'wb-debug',  'SM_AS_TOP_buggy1',  'SM_MAIN_MEM_buggy1',  'SM_RX_TOP',  'b15-bug0',  'c1_DD_s3_f1_e2_v1-bug0',  'c2_DD_s3_f1_e2_v1-bug0',  'c4_DD_s3_f1_e1_v1-bug-gate-0',  'c4_DD_s3_f1_e2_v1-bug0',  'c5_DD_s3_f1_e1_v1-bug-gate-0',  'c5_DD_s3_f1_e1_v2-bug-gate-0',  'c6_DD_s3_f1_e1_v1-bug-gate-0',  'divider_11',  'divider_2',  'divider_5',  'divider_8',  'dividers10',  'dividers_multivec1',  'fpu_multivec1_14',  'i2c_25',  'i2c_26',  'mem_ctrl_27',  'mem_ctrl1',  'mem_ctrl2_mc_dp_28',  'mrisc_mem2wire_29',  'rsdecoder_31',  'rsdecoder_36',  'rsdecoder_37',  'rsdecoder_38',  'rsdecoder_39',  'rsdecoder_40',  'rsdecoder_41',  'rsdecoder1_CSEE_32',  'rsdecoder1_KES_30',  'rsdecoder2',  'rsdecoder4',  'rsdecoder5',  'rsdecoder6',  'rsdecoder_fsm2',  'rsdecoder_multivec1_33',  'rsdecoder_multivec1',  'wb_45',  'wb_46',  'wb1',  'wb2',  'wb_4m8s_47',  'wb_4m8s_48',  'wb_4m8s_49',  'wb_4m8s1',  'wb_4m8s3',  'wb_4m8s4',  'wb_conmax1',  'wb_conmax3']

def genCommands():
    # prefix = '/home/chenwx/workspace/inst/maxsat13/' # on laptop
    prefix = '/s/chopin/b/grad/chenwx/sched/maxsat13/' # on desktop

    commandPrefix = 'cd ~/sched/MiniMAXSAT-AllSol/; ./runem_allsols_one.sh '
    target = 'commands.lst'

# dRange = ['ms_random', 'ms_crafted', 'ms_industrial']
    dRange = ['ms_crafted']

    with open(target, 'w') as f:
        for d in dRange:
            rootdir = prefix+d
            if d=='ms_industrial':
                count = 0
            for root, subFolders, files in os.walk(rootdir):
                files.sort()
                # print subFolders, len(files)
                for i in range(len(files)):
                    filename = files[i]
                    filepath = os.path.join(root, filename)
                    # allPath.append(filepath)
                    # print(filepath)
                    if d == 'ms_industrial':
                        print >>f, "{} {} {}".format(commandPrefix, nicknameList[count], filepath)
                        count = count + 1
                    else:
                        inst = re.search(r'(.+).cnf',filename).group(1)
                        print >>f, "{} {} {}".format(commandPrefix, inst, filepath)        
                    # print "'{}', ".format(inst),
                    # # check duplicate
                    # if inst not in s:
                    #     s.add(inst)
                    # else:
                    #     sleep(1)



def gen_list_of_instances():                    
    dRange = ['ms_random', 'ms_crafted', 'ms_industrial']
    # dRange = ['ms_crafted']
    
    prefix = '/home/chenwx/workspace/inst/maxsat13/' # on laptop
    for d in dRange:
        rootdir = prefix+d
        if d=='ms_industrial':
            with open(d, 'w') as f:
                f.write("\n".join(nicknameList))
        else:
            allfile = []
            for root, subFolders, files in os.walk(rootdir):
                allfile.append(files)
            allfile = sum(allfile,[])
            with open(d, 'w') as f:
                f.write("\n".join(allfile))

