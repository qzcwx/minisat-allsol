#!/bin/bash

SOLVER="../minimaxsat1.0 -F=1"
# SOLVER="../lingeling"

path=$2                        # full path to problem
d=$1                           # short name
i=${d}.cnf

echo "starting" $i "************************"
echo "mkdir ${d} 2>/dev/null"
mkdir ${d} 2>/dev/null
echo "cp ${path} ${d}/${i}"
cp ${path} ${d}/${i}

cp ubcsat ${d}
cd ${d}
cp ${i} ${i}_0
rm ../res/${d} 2>>/dev/null
touch ../res/${d}

echo "$SOLVER ${i} > ${i}.mm"
$SOLVER  ${i} > ${i}.mm
# extract *.mm.out and *.mm.sol
echo "../parse_minimax ${i}.mm"
../parse_minimax ${i}.mm
# based on the solver output on previous instance,
# add clause to generate new instance
# output: cnf_{g} and cnf_{g}.opt
echo "../addclause_minimax ${i}.mm ${i} ${i} 1"
../addclause_minimax ${i}.mm ${i} ${i} 1
t=`cat ${i}.mm.out`         # evaluation of optimum on the original
# instance
k=`cat ${i}_1.opt`          # optimal assignment
j=1
g=1

while [ $k == $t ]
do
    g=$((j+1))
    
    echo "tail -n 1 ${i}_$((j)) > ../res/${d}"
    tail -n 1 ${i}_$((j)) >> ../res/${d}
    echo "eval of original opt" $t
    echo $t > ../res/${i}.opt

    echo "$SOLVER  ${i}_${j} > ${i}_${j}.mm"
    $SOLVER  ${i}_${j} > ${i}_${j}.mm
    echo "../addclause_minimax ${i}_${j}.mm ${i}_${j} ${i} ${g}"
    ../addclause_minimax ${i}_${j}.mm ${i}_${j} ${i} ${g}
    k=`cat ${i}_${j}.opt`
    echo "$j $t opt $k"
    # gzip ${i}_${j}
    j=$((j+1))              # j catches up with g
done
# echo $((j-2)) # -1: remove the effect of the extra adding one; -1: solution found by opt = 1 is not correct
cd ..

# # echo "tail -n $((j-2)) ${d}/${i}_$((j-2)) > res/${i}.sol"
# tail -n $((j-2)) ${d}/${i}_$((j-2)) > res/${i}.sol
# # echo "eval of original opt" $t
# echo $t > res/${i}.opt
